s <- rnorm(1600, 0, 3)
t <- rnorm(1600, 0, 1)

phi <- pi/6

x <- s*cos(phi) - t*sin(phi)
y <- s*sin(phi) + t*cos(phi)

cairo_pdf(file="left.pdf")
   plot(x, y, col="grey", pch=19, xlim = c(-11,11), ylim= c(-11, 11), xlab='', ylab='' )
   grid()
   arrows(0, 0, 9*cos(phi) , 9*sin(phi) , col ='red', angle = 10, length = 0.2, lwd=3)
   arrows(0, 0, -3*sin(phi), 3*cos(phi), col ='blue', angle = 10, length = 0.2, lwd=3)
dev.off()

x <- s*cos(phi) - 3*t*sin(phi)
y <- s*sin(phi) + 3*t*cos(phi)

cairo_pdf(file="right.pdf")
   plot(x, y, col="grey", pch=19, xlim = c(-11,11), ylim= c(-11, 11), xlab='', ylab='' )
   grid()
   arrows(0, 0, 9*cos(phi) , 9*sin(phi) , col ='red', angle = 10, length = 0.2, lwd=3)
   arrows(0, 0, -9*sin(phi), 9*cos(phi), col ='blue', angle = 10, length = 0.2, lwd=3)
dev.off()

