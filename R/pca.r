#!/usr/bin/Rscript

require(lpSolve)

args <- commandArgs(trailingOnly=TRUE);

if (length(args)<3) {
  stop("Usage: pca.r [nom train file] [clusters train file] [output file]", call.=FALSE);
}

train.nom.file <- args[1]
train.class.file <- args[2]

# train.nom.file <- "data/train/car_evaluation.nom.train.csv"
# train.class.file <- "data/train/car_evaluation.clusters.train.csv"

pca.file <- args[3]


train.nom.data <- read.table(train.nom.file, sep=',')
train.nom.factor <- lapply(train.nom.data, as.factor)
train.class.data <- read.table(train.class.file, sep=',')
train.class.factor <- lapply(train.class.data, as.factor)

sum.diff <- function(x, v){
   sum(v != x)
}

sum.sum.diff <- function(v){
   sapply(v, function(x){sum.diff(x, v)})
}

calc.z.beta <- function(data, clusters){
  x<-lapply(split(data, clusters), function(x){ sapply(x, sum.sum.diff)  })
  M <- nrow(clusters)
  rowSums(sapply(x, colSums))/(M*M)
}


calc.opt <- function(data, clusters){
  z.beta <- calc.z.beta(data, clusters)
  m <- ncol(data)
  constr1 <- matrix (1, nrow = 1, ncol=m, byrow = TRUE)
  constr <- rbind(constr1, diag(m))
  opt <- lp("min", z.beta, constr, const.dir=c("=", rep(">=", m)), const.rhs=c(1, rep(0, m)) )
  which.max(opt$solution)
}


m <- ncol(train.nom.data)

opt.first <- calc.opt(train.nom.data, train.class.data)

indices <- 1:m
opt <- opt.first
indices[opt.first] = 0;


for(i in 2:(m-1)){
    opt.n <- calc.opt(train.nom.data[-opt], train.class.data)
    opt.next <- indices[indices>0][opt.n]
    indices[opt.next] = 0;
    opt <- c(opt, opt.next)
#     print("========")
#     print(i)
#     print(indices)
#     print(opt.n)
#     print(opt)
}

opt.last <- which.max(indices)

opt <- c(opt, opt.last)

print(opt)

write.table(t(opt), file=pca.file, sep=',', row.names=FALSE, col.names=FALSE)
