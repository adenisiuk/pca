#!/usr/bin/Rscript

args <- commandArgs(trailingOnly=TRUE);

if (length(args)<2) {
  stop("Usage: split.r [file name] [% for test, rof instalce, 20]", call.=FALSE);
}

data_file <- paste0( 'data/src/', args[1], '.csv' )
train_file <- paste0( 'data/train/', args[1], '.train.csv' )
test_file <- paste0( 'data/test/', args[1], '.test.csv' )

d <- read.table(data_file, header=FALSE, sep = ',')

step <- as.integer(100/as.integer(args[2]))

test_seq <- seq(1, nrow(d), step)

write.table(file = test_file, d[test_seq,], sep=',', col.names=FALSE, row.names=FALSE)
write.table(file = train_file, d[-test_seq,], sep=',', col.names=FALSE, row.names=FALSE)
