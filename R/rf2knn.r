#!/usr/bin/Rscript

args <- commandArgs(trailingOnly=TRUE);

if (length(args)<4) {
  stop("Usage: rf2knn.r [nom train file] [clusters train file] [nom test file] [output file]", call.=FALSE);
}

library(randomForest)

train.nom.file <- args[1]
train.class.file <- args[2]

test.nom.file <- args[3]

knn.file <- args[4]

train.nom.data <- read.table(train.nom.file, sep=',')
train.nom.factor <- lapply(train.nom.data, as.factor)
train.class.data <- read.table(train.class.file, sep=',')
train.class.factor <- lapply(train.class.data, as.factor)

train.data <- cbind(data.frame(train.class.factor), data.frame(train.nom.factor))
names(train.data) <- paste0("V", 1:ncol(train.data))
names(train.data)[1] <- 'First'

test.nom.data <- read.table(test.nom.file, sep=',')
test.nom.factor <- lapply(test.nom.data, as.factor)

test.data <- cbind(test.nom.factor)
test.data <- data.frame(test.nom.factor)
names(test.data) <- names(train.data)[-1]

trained.rf <- randomForest( First ~  ., data=train.data)

test.data <- rbind(train.data[1, -1] , test.data)
knn <- predict(trained.rf, test.data[-1,], type="response")

write.table(as.numeric(knn), 
            file=knn.file, 
            sep=',', 
            row.names=FALSE, 
            col.names=FALSE)
