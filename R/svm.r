#!/usr/bin/Rscript

args <- commandArgs(trailingOnly=TRUE);

# args <- c("data/train/car_evaluation.nom.train.csv", "data/train/car_evaluation.clusters.train.csv", "data/test/car_evaluation.nom.test.csv", "data/test/car_evaluation.clusters.test.csv", "pca/car_evaluation.pca.csv", "results/car_evaluation.rf.pca.csv")

if (length(args)<7) {
  stop("Usage: svm.r [nom train file] [clusters train file] [nom test file] [clusters test file]  [pca_features_file] [mode (pca/acp/sample)] [output file] ", call.=FALSE);
}

library(e1071)

train.nom.file <- args[1]
train.class.file <- args[2]

test.nom.file <- args[3]
test.class.file <- args[4]

pca.features.file <- args[5]

pca.mode <- args[6]

out.file <- args[7]

# weights 

pca.features.data <- read.table(pca.features.file, sep=',', header=FALSE)

train.nom.data <- read.table(train.nom.file, sep=',')
train.nom.factor <- lapply(train.nom.data, as.factor)
train.class.data <- read.table(train.class.file, sep=',')
train.class.factor <- lapply(train.class.data, as.factor)

train.data <- cbind(data.frame(train.class.factor), data.frame(train.nom.factor))
names(train.data) <- paste0("V", 1:ncol(train.data))
names(train.data)[1] <- 'First'


test.nom.data <- read.table(test.nom.file, sep=',')
test.nom.factor <- lapply(test.nom.data, as.factor)

test.class.data <- read.table(test.class.file, sep=',')
test.class.factor <- sapply(test.class.data, as.factor)

test.data <- as.data.frame(test.nom.factor)
names(test.data) <- names(train.data)[-1]


ord <- as.numeric(pca.features.data)
if( pca.mode == "pca"){
  ord <- rev(ord)
} else if( pca.mode == "sample"){
  ord <- sample(ord)
} else if( pca.mode != "acp"){
  stop(paste("unknown mode:", pca.mode))
}


test.ord <- test.data[,ord]
train.ord <- train.data[,c(1, (1+ord)) ]

n_of_classes <- max(train.class.data)


# write.table(file=out.file, t(c('weights:', sub('.', ',', weights[ord], fixed=TRUE)) ), sep=';', row.names=FALSE,  col.names=FALSE)
write.table(file=out.file,t(c(0, ord)), sep=',', row.names=FALSE,  col.names=FALSE, append=FALSE)



# f100 <- data.frame()
# for(j in 1:n_of_classes){
#   f100[j,1] <- j
#   for ( i in 2:length(ord)){
#       f100[j,i] <- 0
#   }
# }
# f100[n_of_classes+1,1] <- 0
# for ( i in 2:length(ord)){
#     f100[n_of_classes+1,i] <- 0
# }



f <- data.frame()
for(j in 1:n_of_classes){
    f[j,1] <- j
}
f[n_of_classes+1,1] <- 0


# for(n_of in 1:100){
  total_true_positive = 0;
  total_false_positive = 0;
  total_false_negative = 0;
#   cat(paste0('[',n_of,'] '))
  for ( i in 2:length(ord)){
    trained.svm <- svm( First ~  ., data=train.ord[,c(1, i:ncol(train.ord))])
    test.data <- rbind(train.ord[1, i:ncol(train.ord)] , test.ord[, (i-1):ncol(test.ord)])
    knn <- predict(trained.svm, test.data[-1,], type="response")
    knn.num <- as.numeric(as.character(knn))
    for(j in 1:n_of_classes){
      true_positive = sum( test.class.data==j & knn.num==j )
      total_true_positive = total_true_positive + true_positive
      false_positive = sum( test.class.data!=j & knn.num==j )
      total_false_positive =  total_false_positive + false_positive
      false_negative = sum( test.class.data==j & knn.num!=j )
      total_false_negative =  total_false_negative + false_negative
      precision = 0
      if (true_positive>0){
        precision = true_positive/(true_positive + false_positive)
      }
      recall = 0
      if( true_positive>0 ){
        recall = true_positive/(true_positive + false_negative)
      }
      f1 = 0
      if( precision+recall>0 ){
        f1 =  2.0*precision*recall/(precision+recall);
      }
      f[j,i] <- f1
#       f100[j,i] <- f100[j,i]+f1
    }
    total_precision = 0
    if (total_true_positive>0){
      total_precision = total_true_positive/(total_true_positive + total_false_positive)
    }
    total_recall = 0
    if( total_true_positive>0 ){
      total_recall = total_true_positive/(total_true_positive + total_false_negative)
    }
    total_f1 = 0
    if( total_precision+recall>0 ){
      total_f1 =  2.0*total_precision*total_recall/(total_precision+total_recall);
    }
    f[n_of_classes+1,i] <- total_f1
#     f100[n_of_classes+1,i] <- f100[n_of_classes+1,i] + total_f1
    cat(paste0(' ',total_f1))
  }
  cat("\n")
# }

write.table(file=out.file, round(f, digits=4), sep=',', row.names=FALSE,  col.names=FALSE, append=TRUE)


# 
# trained.rf <- randomForest( First ~  ., data=train.data)
# test.data <- rbind(train.data[1, -1] , test.data)
# knn <- predict(trained.rf, test.data[-1,],type="response")
# 
# write.table(as.numeric(knn), 
#             file=knn.file, 
#             sep=',', 
#             row.names=FALSE, 
#             col.names=FALSE)
