#!/usr/bin/Rscript

args <- commandArgs(trailingOnly=TRUE);

# args <- c("results/car_evaluation.rf.pca.csv", "results/car_evaluation.rf.sample.csv", "results/car_evaluation.2.rf.sample.csv", "results/car_evaluation.rf.acp.csv", "results/car_evaluation.rf.pdf")

if (length(args)<5) {
  stop("Usage: xgboost.r [pca file] [sample file] [sample2 file] [acp file]  [output file] ", call.=FALSE)
}

pca.file <- args[1]
sample.file <- args[2]
sample2.file <- args[3]
acp.file <- args[4]

out.file <- args[5]

method <- substring(out.file, nchar(out.file)-6)

if( method == ".rf.pdf" ){
  method <- "Random Forest"
} else if(method == "svm.pdf" ){
  method <- "SVM"
} else if(method == "ost.pdf" ){
  method <- "xGBoost"
} else{
  stop(paste0("Unknown method: ", method))
}

pca.data <- read.table(pca.file, sep=',', header=FALSE, fill=TRUE)
acp.data <- read.table(acp.file, sep=',', header=FALSE, fill=TRUE)
sample.data <- read.table(sample.file, sep=',', header=FALSE, fill=TRUE)
sample2.data <- read.table(sample2.file, sep=',', header=FALSE, fill=TRUE)

mat <- rbind(
  pca.data[nrow(pca.data), c(-1, -ncol(pca.data))],
  acp.data[nrow(acp.data), c(-1, -ncol(acp.data))], 
  sample.data[nrow(sample.data), c(-1, -ncol(sample.data))],
  sample2.data[nrow(sample2.data), c(-1, -ncol(sample2.data))])

  
cairo_pdf(file=out.file)

matplot(t(mat), type='l', col=c("red", "blue", "green", "green"), lty='solid', lwd=2, ylab="F1 Score", main=method, xlab="Number of features", axes=FALSE)
box(col = "black")
axis(2)
axis(side=1,at=1:ncol(mat),labels=(ncol(mat)+1):2)
legend("topright", col=c("red", "blue", "green"), legend=c('pca', 'acp', 'sample'), pch=15)

dev.off()
