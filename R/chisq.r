#!/usr/bin/Rscript

args <- commandArgs(trailingOnly=TRUE);

# args <- c("data/train/car_evaluation.nom.train.csv", "results/car_evaluation.chisq.csv")

if (length(args)<2) {
  stop("Usage: chisq.r [nom train file] [output file] ", call.=FALSE);
}

train.nom.file <- args[1]

out.file <- args[2]


nom.data <- read.table(train.nom.file, sep=',')
nom.fac <- as.data.frame(lapply(nom.data, as.factor))
n.nom <- ncol(nom.fac)

cor.chisq <- matrix(dimnames=list(colnames(nom.fac), colnames(nom.fac)), nrow=n.nom, ncol=n.nom )
for(i in 1:n.nom){
   for(j in 1:n.nom){
      cor.chisq[i,j] <- chisq.test(table(nom.data[[j]],nom.fac[[i]]), simulate.p.value=TRUE)$p.value
   }
}

print(cor.chisq)

write.table(file=out.file, round(cor.chisq, digits=4), sep=',', row.names=FALSE,  col.names=FALSE)


# 
# trained.rf <- randomForest( First ~  ., data=train.data)
# test.data <- rbind(train.data[1, -1] , test.data)
# knn <- predict(trained.rf, test.data[-1,],type="response")
# 
# write.table(as.numeric(knn), 
#             file=knn.file, 
#             sep=',', 
#             row.names=FALSE, 
#             col.names=FALSE)
